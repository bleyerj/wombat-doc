A first example : 2D truss structure
====================================


This first example is implemented in a single Python file,
:download:`cours_bar2D.py`, which is used in the first class
session on truss structures modeled using :class:`Bar2D <bar2D.Bar2D>` elements. The problem we aim at modelling is the following :

.. image:: _pic/cours_bar2D_problem.png

This demo illustrates how to:

* :ref:`define nodes and elements describing the structure <sec-nodes-elem>`
* :ref:`build a mesh consisting of a connection of elements and assign material and section properties to a model <sec-mesh-model>`
* :ref:`define boundary and loading conditions <sec-bcs-loading>`
* :ref:`assemble the global stiffness matrix, connection matrix, external force vector and solve the associated linear problem <sec-assembly>`
* :ref:`post-process and plot the solution <sec-postprocess>`


First, we import the **WomBat** module : 

.. code-block:: python

	from wombat import *  

.. _sec-nodes-elem:

Defining nodes and elements
___________________________

Defining nodes
~~~~~~~~~~~~~~

Two-dimensional nodes are created using the :class:`Node2D <node.Node2D>` constructor accepting a list or a Numpy ndarray of its :math:`(x,y)` coordinates e.g. :code:`new_node = Node2D([x,y])`. The three nodes of the considered structure are therefore created as:

.. code-block:: python

	n0 = Node2D([0.,0.])    # first node of coordinates (0,0)
	n1 = Node2D([1.,0.])    # second node of coordinates (1,0)
	n2 = Node2D([0.,-1.])   # and the third one 

Defining elements
~~~~~~~~~~~~~~~~~

Elements are created using their own constructor which accepts a list of nodes. For the :class:`Bar2D <bar2D.Bar2D>` element, only two nodes are required e.g. :code:`new_element = Bar2D([na,nb])` creates a new ``Bar2D`` element connecting node ``na`` to ``nb`` (the order defines the element orientation). Implicitly, this element belongs to the physical group tagged as ``1``, an optional argument ``tag`` can be added in the element definition to specify an other tag (which can be a string or an int) e.g. :code:`new_element = Bar2D([na,nb],tag=2)` or :code:`new_element = Bar2D([na,nb],tag="other_tag")`. The three elements of the structure are hence created as (implicitly tagged as ``1``):

.. code-block:: python

	el0 = Bar2D([n0,n1])   # creation of an element connecting node n0 to n1
	el1 = Bar2D([n1,n2])   # a second connecting n1 to n2
	el2 = Bar2D([n0,n2])   # and a third one connecting n0 to n2

.. note :: Note that if two nodes with the same coordinates are separately created e.g. :code:`na = Node2D([0.,0.])` and :code:`nb = Node2D([0.,0.])`, ``na`` and ``nb`` will be different objects and will be associated to different degrees of freedom. Hence, if one element is connecting ``na`` and another is connecting ``nb``, the two elements will not be mechanically connected.

.. _sec-mesh-model:

Building a mesh and assigning material and section properties to a model
________________________________________________________________________

Building a mesh
~~~~~~~~~~~~~~~

The mesh merely consists of a collection of elements which are passed as a list to the :class:`Mesh <mesh.Mesh>` constructor e.g. :code:`mesh = Mesh([el1,el2])` for a mesh consisting of two elements named ``el1`` and ``el2``. An error will be raised if all elements are not of the same kind. For the considered example, the mesh construction reads as :

.. code-block:: python

   mesh = Mesh([el0,el1,el2])

Material properties
~~~~~~~~~~~~~~~~~~~

The class :class:`LinearElastic <material.LinearElastic>` can be used to define a linear isotropic elastic material by providing its Young's modulus (Poisson ratio can be ignored for strctural elements) as follows : :code:`mat = LinearElastic(E=1e3)`.

Cross-section properties
~~~~~~~~~~~~~~~~~~~~~~~~

Structural elements also need geometrical characteristics of their cross-section. This is achieved through the use of the :class:`BeamSection <geometric_caract.BeamSection>` class which can be instantiated by providing directly the cross section area :math:`S` (the bending inertia :math:`I` is ignored for ``Bar2D`` elements) e.g. :code:`sect = BeamSection(S=1.)`. Alternatively, it is also possible to define a rectangular cross-section by giving its width :math:`b` and height :math:`h` as follows : :code:`sect = BeamSection().rect(0.5,1.)`, the cross-section area and bending inertia will then be computed from :math:`b` and :math:`h`. Other standard geometrical cross-sections (e.g. circular, annular, I, etc.) could be easily added to the :class:`BeamSection <geometric_caract.BeamSection>` class  using the same structure as the :func:`rect() <geometric_caract.BeamSection.rect>` method.

Defining a model and assigning properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Finally, a model collects the information of the mesh and material properties (and possibly cross-section properties) which are given as arguments to  the :class:`Model <model.Model>` constructor e.g. :code:`model = Model(mesh,mat,sect)`. Using this default instantiation, ``mat`` and ``sect`` are affected to all elements of the mesh. 

If the mesh consists of two different kinds of elements tagged for instance ``1`` and ``other_tag``, and if two different kinds of materials ``mat`` and ``mat2`` and cross-section properties ``sect`` and ``sect2`` have been defined, ``mat`` and ``sect`` can for instance be affected first to all elements and the :func:`affect_property <model.Model.affect_property>` method can then be used to affect ``mat2`` and ``sect2`` to elements tagged as ``other_tag`` :

.. code-block:: python
   
   model = Model(mesh,mat,sect)	                 # affects mat and sect to all elements, irrespective of their tags
   model.affect_property(mat2,tag="other_tag")   # affects mat2 to elements tagged as "other_tag"
   model.affect_property(sect2,tag="other_tag")  # does the same for sect2

.. note :: Note that :func:`affect_property <model.Model.affect_property>` can use directly a list of elements as an argument instead of a tag. If both a list of elements and a tag are given, elements belonging to the list and tagged as the argument will be selected by the method.

For the considered example, the material and geometric properties and model definitions read as :

.. code-block:: python

   mat = LinearElastic(E=1000)  
   sect = BeamSection(A=1.)    
   model = Model(mesh,mat,sect)


.. _sec-bcs-loading:

Defining boundary and loading conditions
________________________________________

Boundary conditions
~~~~~~~~~~~~~~~~~~~

Boundary conditions are defined using the :class:`Connections <connections.Connections>` class which is simply instantiated as ``appuis = Connections()``. Different boundary conditions can then be added to the ``appuis`` instance using the method :func:`add_imposed_displ <connections.Connections.add_imposed_displ>` which requires a list of nodes and values for the different displacement conditions. If nothing is given for a specific displacement component, it will remain free. For the considered problem, we can first fix both displacements for node ``n0`` and then fix only the horizontal displacement for ``n2`` as follows:

.. code-block:: python

   appuis = Connections()                     # creation of an empty Connections object for boundary conditions
   appuis.add_imposed_displ(n0,ux=0,uy=0)     # fixed displacement for n0 (ux=uy=0)
   appuis.add_imposed_displ(n2,ux=0)          # fixed horizontal displacement for n2 (ux=0, uy is free)

Alternatively, it is possible to define both conditions in a single line by giving a list containing ``n0`` and ``n2`` and a list of values for :math:`u_x` and :math:`u_y` for the corresponding nodes. A free displacement condition is specified using the Python keyword ``None`` :

.. code-block:: python

   appuis = Connections()                                # creation of an empty Connections object for boundary conditions
   appuis.add_imposed_displ([n0,n2],ux=0,uy=[0,None])    # alternative way of defining the bcs

if one value only is specified for a given component, it will be taken into account for all nodes of the list

Loading conditions
~~~~~~~~~~~~~~~~~~

Loading conditions are defined in a similar way using the :class:`ExtForce <forces.ExtForce>` class which is also simply instantiated as ``forces = ExtForce()``. Different loading conditions can then be applied to the ``forces`` instance using the method :func:`add_concentrated_forces <forces.ExtForce.add_concentrated_forces>` which requires a list of nodes and values for the different concentrated force components :math:`F_x` and :math:`F_y`. The way of passing the arguments is similar to the :func:`add_imposed_displ <connections.Connections.add_imposed_displ>` method. When nothing is specified for a given component this corresponds to a zero value. The loading of the considered problem can then be defined as :

.. code-block:: python

   forces = ExtForce()                              # creation of an empy ExtForce object for loading conditions
   forces.add_concentrated_forces(n1,Fy=-1)         # adds a fixed concentrated force (0,-1) at n1

.. _sec-assembly:

Assembly step and resolution of associated linear system
_________________________________________________________

Assembling the stiffness matrix
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once a model has been defined, the global stiffness matrix :math:`[K]` is assembled using the :func:`assembl_stiffness_matrix <finite_elements.assembl_stiffness_matrix>` function as follows : 

.. code-block:: python
   
   K = assembl_stiffness_matrix(model)

Assembling the external force vector
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Similarly, the external force vector :math:`\\{F\\}` is assembled by providing a :class:`ExtForce <forces.ExtForce>` instance (e.g. ``forces``) to the function :func:`assembl_external_forces <finite_elements.assembl_external_forces>` as follows : 

.. code-block:: python

   F = assembl_external_forces(forces,model)

Assembling the connection matrix and imposed displacement vector
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Boundary conditions in **WomBat** are taken into account using Lagrange multipliers. For this a connection matrix :math:`[L]` and the imposed displacement vector :math:`\\{U_d\\}` need to be formed. This step is realized by providing a :class:`Connections <connections.Connections>` instance (e.g. ``appuis``) to the :func:`assembl_connections <finite_elements.assembl_connections>` as follows :

.. code-block:: python

   L,Ud = assembl_connections(appuis,model)

Resolution of the linear system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The function :func:`solve <finite_elements.solve>` forms and solves the following augmented linear system : 

.. math:: 
   \begin{bmatrix} K & L^T \\ L & 0 \end{bmatrix}\begin{Bmatrix} U \\ \lambda \end{Bmatrix}=\begin{Bmatrix} F \\ U_d \end{Bmatrix}

It returns the displacement vector :math:`\{U\}` and vector of Lagrange multipliers :math:`\{\lambda\}` solutions of the previous system :

.. code-block:: python

   U,lamb = solve(K,F,L,Ud)

.. _sec-postprocess:

Post-processing and plotting the solution
__________________________________________

Extracting components and computing stresses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The displacement vector :math:`\{U\}` collects the degrees of freedom of each node in a Numpy ndarray. For a :class:`Bar2D <bar2D.Bar2D>` element, each node has two degrees of freedom, the displacement component :math:`u_x` along the :math:`x`-axis and the component :math:`u_y` along the :math:`y`-axis. Both components can be extracted from ``U`` using a slice with a step size of 2, starting from 0 for :math:`u_x` and from 1 for :math:`u_y` :

.. code-block:: python

   Ux = U[0::2]               # extraction of x-component
   Uy = U[1::2]               # extraction of y-component

The generalized stresses of a given element type can be computed using the :func:`stresses <finite_elements.stresses>` function, which takes the displacement vector ``U`` and the model as a parameter. This function returns a Numpy ndarray containing the generalized stresses of the element. For the :class:`Bar2D <bar2D.Bar2D>` element, there is only one normal force :math:`N` per element.

.. code-block:: python

   N = stresses(U,model)

Plotting the mesh
~~~~~~~~~~~~~~~~~

**WomBat** offers some basic plotting functionalities using the :class:`Figure <post_process.Figure>` class. In all cases, a ``Figure`` is first intantiated using the dedicated constructor. A figure number and title can also be provided to the constructor as follows :code:`fig = Figure(1,"My_figure_title")`. The :class:`Figure <post_process.Figure>` class possess many methods offering different plotting functionalities. The :func:`show <post_process.Figure.show>` must always be called to draw the resulting plot : :code:`fig.show()`. By default, the code execution is suspended until the user closes the figure window. The ``block`` keyword can be set to ``False`` so that the figure remains open and the code continues its execution.

The structure mesh can be plotted using the :func:`plot <post_process.Figure.plot>` method using the mesh as an input argument, optional arguments such as ``nodelabels`` (resp. ``elemlabels``) can be set to ``True`` to plot the nodes (resp. elements) id numbers. ``nodesymbols`` can be used to plot (or not) the nodes using circles. For the present example, the following snippet :

.. code-block:: python

   fig = Figure(1,"Mesh")    # creation of a Figure
   fig.plot(mesh,nodelabels=True,elemlabels=True)    # plots the mesh and nodes and elements id numbers
   fig.show() 		     # force the drawing of the figure

gives the following figure :

.. image:: _pic/cours_bar2D_mesh.png

Plotting boundary conditions and the deformed shape
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Boundary conditions can be plotted using the :func:`plot_bc <post_process.Figure.plot_bc>` method from the mesh and a :class:`Connections <connections.Connections>` object. Fixed horizontal displacements are represented using vertical green segments, for fixed vertical displacement, horizontal green segments are used.

The deformed shape can be plotted using the :func:`plot_def <post_process.Figure.plot_def>` method using the displacement vector ``U``, in addition to the mesh, as an input parameter. A numerical parameter controlling the amplification factor of the deformed shape can also be specified. By default, the undeformed mesh is also plotted on the same figure, this feature can be deactivated using the optional keyword argument ``undef=False``. For the present example, the following snippet :


.. code-block:: python

   fig = Figure(2,"Deformed shape")
   fig.plot_bc(mesh,appuis)  # plots boundary conditions
   fig.plot_def(mesh,U,40)   # plots deformed shape (amplitude x40)
   fig.show()                

gives the following figure :

.. image:: _pic/cours_bar2D_deformed_shape.png
.. literalinclude:: cours_bar2D.py
