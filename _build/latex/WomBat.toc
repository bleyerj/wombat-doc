\select@language {english}
\contentsline {chapter}{\numberline {1}Tutorials}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}A first example : 2D truss structure}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Defining nodes and elements}{4}{subsection.1.1.1}
\contentsline {subsubsection}{Defining nodes}{4}{subsubsection*.3}
\contentsline {subsubsection}{Defining elements}{4}{subsubsection*.4}
\contentsline {subsection}{\numberline {1.1.2}Building a mesh and assigning material and section properties to a model}{4}{subsection.1.1.2}
\contentsline {subsubsection}{Building a mesh}{4}{subsubsection*.5}
\contentsline {subsubsection}{Material properties}{4}{subsubsection*.6}
\contentsline {subsubsection}{Cross-section properties}{4}{subsubsection*.7}
\contentsline {subsubsection}{Defining a model and assigning properties}{5}{subsubsection*.8}
\contentsline {subsection}{\numberline {1.1.3}Defining boundary and loading conditions}{5}{subsection.1.1.3}
\contentsline {subsubsection}{Boundary conditions}{5}{subsubsection*.9}
\contentsline {subsubsection}{Loading conditions}{6}{subsubsection*.10}
\contentsline {subsection}{\numberline {1.1.4}Assembly step and resolution of associated linear system}{6}{subsection.1.1.4}
\contentsline {subsubsection}{Assembling the stiffness matrix}{6}{subsubsection*.11}
\contentsline {subsubsection}{Assembling the external force vector}{6}{subsubsection*.12}
\contentsline {subsubsection}{Assembling the connection matrix and imposed displacement vector}{6}{subsubsection*.13}
\contentsline {subsubsection}{Resolution of the linear system}{7}{subsubsection*.14}
\contentsline {subsection}{\numberline {1.1.5}Post-processing and plotting the solution}{7}{subsection.1.1.5}
\contentsline {subsubsection}{Extracting components and computing stresses}{7}{subsubsection*.15}
\contentsline {subsubsection}{Plotting the mesh}{7}{subsubsection*.16}
\contentsline {subsubsection}{Plotting boundary conditions and the deformed shape}{8}{subsubsection*.17}
\contentsline {chapter}{\numberline {2}WomBat modules}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}\sphinxstyleliteralintitle {node}}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}\sphinxstyleliteralintitle {element}}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\sphinxstyleliteralintitle {bar2D}}{12}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\sphinxstyleliteralintitle {beam2D}}{14}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}\sphinxstyleliteralintitle {solidT3}}{16}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}\sphinxstyleliteralintitle {solidT6}}{17}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}\sphinxstyleliteralintitle {mesh}}{18}{section.2.3}
\contentsline {section}{\numberline {2.4}\sphinxstyleliteralintitle {geometric\_caract}}{19}{section.2.4}
\contentsline {section}{\numberline {2.5}\sphinxstyleliteralintitle {material}}{20}{section.2.5}
\contentsline {section}{\numberline {2.6}\sphinxstyleliteralintitle {model}}{21}{section.2.6}
\contentsline {section}{\numberline {2.7}\sphinxstyleliteralintitle {connections}}{22}{section.2.7}
\contentsline {section}{\numberline {2.8}\sphinxstyleliteralintitle {forces}}{24}{section.2.8}
\contentsline {section}{\numberline {2.9}\sphinxstyleliteralintitle {finite\_elements}}{25}{section.2.9}
\contentsline {section}{\numberline {2.10}\sphinxstyleliteralintitle {finite\_elements\_sparse}}{26}{section.2.10}
\contentsline {section}{\numberline {2.11}\sphinxstyleliteralintitle {nonlinear}}{27}{section.2.11}
\contentsline {section}{\numberline {2.12}\sphinxstyleliteralintitle {time\_integration}}{27}{section.2.12}
\contentsline {section}{\numberline {2.13}\sphinxstyleliteralintitle {post\_process}}{27}{section.2.13}
\contentsline {section}{\numberline {2.14}\sphinxstyleliteralintitle {input}}{29}{section.2.14}
\contentsline {section}{\numberline {2.15}\sphinxstyleliteralintitle {utils}}{30}{section.2.15}
\contentsline {chapter}{\numberline {3}Indices and tables}{31}{chapter.3}
\contentsline {chapter}{Python Module Index}{33}{section*.255}
