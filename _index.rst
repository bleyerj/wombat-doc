.. WomBat documentation master file, created by
   sphinx-quickstart on Fri Aug  4 23:18:23 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WomBat's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: wombat

.. autosummary::
   :toctree: _autosummary

   wombat
   node
   wombat.element

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
