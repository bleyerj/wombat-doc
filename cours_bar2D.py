# -*- coding: utf-8 -*-
"""
Séance 1
Potence (exemple du cours)

This file is part of the WomBat finite element code
used for the Civil Engineering Finite Element Course
of Ecole des Ponts ParisTech 2017-2018

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from wombat import *

# Définitions des noeuds
n0 = Node2D([0.,0.])    # creation d'un noeud 2D de coordonnées (0,0)
n1 = Node2D([1.,0.])    # un deuxième de coordonnées (1,0)
n2 = Node2D([0.,-1.])   # et un troisième

# Définitions des éléments (de type Bar2D)
el0 = Bar2D([n0,n1])   # création d'un élément reliant les noeuds n0 et n1
el1 = Bar2D([n1,n2])   # un deuxième reliant n1 a n2
el2 = Bar2D([n0,n2])   # et un troisième reliant n0 a n2

mesh = Mesh([el0,el1,el2])    # création d'un objet maillage
                              # regroupant les éléments définis précédemment

# Définition d'un matériau linéaire élastique
mat = LinearElastic(E=1000)    #  module d'Young E = 1000
# Définition d'une section géométrique
sect = BeamSection(S=1.)       # section S = 1

# Construction d'un modèle (affectation de 'mat' et 'sect' à l'ensemble du maillage)
model = Model(mesh,mat,sect)

# Définition du chargement
forces = ExtForce()                              # création d'un objet effort extérieur
forces.add_concentrated_forces(n1,Fy=-1)         # ajout d'une force concentrée (0,-1) en n1

# Définition des conditions aux limites
appuis = Connections()     # création d'un objet Connections (CL, liaisons)
appuis.add_imposed_displ([n0,n2],ux=[0,0],uy=[0,None])     # déplacement imposé ux=uy=0
                                                          # en n0 et ux=0 en n2
# Phase d'assemblage
K = assembl_stiffness_matrix(model)            # assemblage de la rigidité
L,Ud = assembl_connections(appuis,model)        # assemblage des liaisons
F = assembl_external_forces(forces,model)       # assemblage des efforts extérieurs

# Résolution du système : déplacement U, multiplicateurs de Lagrange lamb
U,lamb = solve(K,F,L,Ud)
Ux = U[0::2]               # extraction de la composante suivant x
Uy = U[1::2]               # extraction de la composante suivant y

# Post-traitement/visualisation
fig = Figure(1,"Mesh")    # creation d'une figure
fig.plot(mesh,nodelabels=True,elemlabels=True)    # dessin du maillage et des numéros de noeuds et éléments
fig.show()     # commande d'affichage

fig = Figure(2,"Deformed shape")
fig.plot_bc(mesh,appuis)  # dessin des conditions aux limites
fig.plot_def(mesh,U,40)   # dessin de la deformee (amplitude x40)
fig.show()                # commande d'affichage

# Validation
print "Displacement node 1",(Ux[1],Uy[1])
EA = mat.Young_modulus*sect.area
print "Analytic solution",(1./EA,-2*(1+2**0.5)/EA) # solution analytique

# Calcul des efforts intérieurs (effort normal uniquement)
N = stresses(U,model)
fig = Figure(3,"Normal force")
fig.plot_field_diagrams(mesh,N) # trace de l'effort normal
fig.show()
